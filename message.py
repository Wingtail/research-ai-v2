import zerorpc
import searcherKR as searcher

class Ask(object):
    def __init__(self):
        self.searcher = searcher.WikiSearch()
    def ask(self, question):
        return self.searcher.ask(question)

s = zerorpc.Server(Ask())
s.bind("tcp://0.0.0.0:4545")
s.run()
