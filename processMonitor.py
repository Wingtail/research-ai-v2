import os, psutil
pid = input("PID: ")
print(pid)

ps = psutil.Process(int(pid))
memoryUse = None
while True:
    if(memoryUse != ps.memory_full_info().uss):
        memoryUse = ps.memory_full_info().uss
        print(memoryUse >> 20," mb")
