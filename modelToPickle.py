import pickle
from pytorch_pretrained_bert import BertForQuestionAnswering

model = BertForQuestionAnswering.from_pretrained('./model/')
model.eval()

pickle.dump(model, open('bertKor.p', 'wb'))

