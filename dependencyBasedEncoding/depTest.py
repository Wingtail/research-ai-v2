from preProcessor import PreProcess
dep = PreProcess()

deps = dep.preProcess("Genes encode the information needed by cells for the synthesis of proteins, which in turn play a central role in influencing the final phenotype of the organism. Genetics provides research tools used in the investigation of the function of a particular gene, or the analysis of genetic interactions. Within organisms, genetic information is physically represented as chromosomes, within which it is represented by a particular sequence of amino acids in particular DNA molecules.")

print(deps[0])
deps[1][0].printAllConnections('',0)
