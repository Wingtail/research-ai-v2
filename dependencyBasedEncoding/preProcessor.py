import stanfordnlp
from queue import Queue

class PreProcess:
    def __init__(self):
        print('initializing stanfordnlp')
        self.nlp = stanfordnlp.Pipeline()
        self.subjects = []
        self.rawText = None
        print('complete')

    def preProcess(self, text):
        doc = self.nlp(text)
        dependencies = self.getDependencies(doc)
        lemmatized = self.getLemmatized(doc)
        print('lemma: ', lemmatized)
        return (lemmatized, dependencies)

    def getLemmatized(self, doc):
        paragraph = []
        print('lemmatizing')
        for i in range(0,len(doc.sentences)):
            words = [word.lemma for word in doc.sentences[i].words]
            paragraph.append(' '.join(words))
        paragraph = ' '.join(paragraph)
        print('completed')
        return paragraph

    def getDependencies(self, doc):
        self.subjects = []
        print('evaluating dependency')
        print(doc.sentences[0].words)
        dependencyTrees = []
        print('constructing dependency tree')
        for i in range(0,len(doc.sentences)):
            nodes = []
            dependencies = []
            nodes.append(Node('ROOT', 0, ""))
            for dependency in doc.sentences[i].dependencies:
                node = Node(dependency[2].text, dependency[2].index, dependency[2].dependency_relation)
                if(node.dependency == 'nsubj'):
                    self.subjects.append(node)
                nodes.append(node)

            for dependency in doc.sentences[0].dependencies:
                parent = None
                child = None
                for node in nodes:
                    if(int(node.id)==int(dependency[2].governor)):
                        parent = node
                    if(int(node.id)==int(dependency[2].index)):
                        child = node
                    if(parent != None and child != None):
                        parent.connections.append((child, dependency[2].dependency_relation))
                        break
            q = Queue()
            q.put(nodes[0])
            while(not q.empty()):
                node = q.get_nowait()
                for child in node.connections:
                    child[0].rootDist = node.rootDist+1
                    q.put(child[0])
            dependencyTrees.append(nodes[0])
        print('complete')
        return dependencyTrees

class Node:
    def __init__(self, name, id, dependency):
        self.connections = [] #dependency parse
        self.name = name
        self.id = id
        self.dependency = dependency
        self.attributes = []
        self.relationships = []
        self.rootDist = 0

    def indent(self, num):
        for i in range(num-1):
            print('     ', end="")
        print('---- ', end="")
        return

    def printAllConnections(self, attr, count):
        self.indent(count)
        print(self.name, " : ", attr, ' root dist: ', self.rootDist)
        count += 1
        for connection in self.connections:
            connection[0].printAllConnections(connection[1], count)

        return self.connections

    def searchDependency(self, dependency):
        que = []
        #from node's connections, search for the dependency
        for connection in self.connections:
            if(connection[1].find(dependency) > -1):
                return connection[0]
        return None

    def searchDependencies(self,dependencies):
        dependencys = []
        for connection in self.connections:
            for dependency in dependencies:
                if(connection[1].find(dependency) > -1):
                    dependencys.append(connection[0])
        return [i for i in dependencys if i != None]

    def getAttributes(self):
        attribute = self.searchDependency('amod')
        if attribute not in self.attributes:
            self.attributes.append(attribute)

