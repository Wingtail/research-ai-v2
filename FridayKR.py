from searcherKR import WikiSearch

from gtts import gTTS
import speech_recognition as sr
import os
import webbrowser
#import smtplib
from pygame import mixer
import pygame as pg


class Assistant:
    def __init__(self):
        self.mic = sr.Microphone()
        self.r = sr.Recognizer()
        self.wiki = WikiSearch()
        with self.mic as source:
            self.r.energy_threshold = 400

    def talk(self, audio):
        print(audio)
        mixer.init(27000)
        speech = gTTS(text=audio, lang='ko')
        speech.save('audio.mp3')
        clock = pg.time.Clock()
        try:
            mixer.music.load('audio.mp3')
        except pg.error:
            print('Speech error!')
        mixer.music.play()
        while mixer.music.get_busy():
            clock.tick(30)

    def speech2T(self):
        with self.mic as source:
            print('Ready listening!')
            #self.r.adjust_for_ambient_noise(self.mic, duration=0.5)
            audio = self.r.listen(source)
        try:
            query = self.r.recognize_google(audio, language='ko')
            print(query)

        except sr.UnknownValueError:
            print('adjusting sound')
            self.r.adjust_for_ambient_noise(source)
            command = self.speech2T()
        query = query.split(" ")
        query = ' '.join(query)
        return query

    def activate(self, phrase):
        with self.mic as source:
            try:
                #self.r.adjust_for_ambient_noise(self.mic, duration=0.5)
                audio = self.r.listen(source)
                text = self.r.recognize_google(audio, language='ko')
                print(text)
                if (text == phrase):
                    return True
                else:
                    return False
            except sr.UnknownValueError:
                self.r.adjust_for_ambient_noise(source)
                print('adjusting sound')

    def run(self):
        #Run BERT or any other classifier that comprehends speech
        #cross reference which command does the query best match
        self.talk('구동완료')
        print('PID: ',os.getpid())
        while True:
            #if(self.friend == None):
            #    self.talk("Hi, you are new")
            #    self.talk("I can listen when the message Ready to listen pops up")
            #    self.talk("Lets see if you understand: What's your name")
            #    self.friend = self.speech2T()
            #    speech = "Hi, "+self.friend+" I am Friday"
            #    self.talk(speech)
            #    self.talk("Call 'friday' and I will listen to your question and search it")
            #    self.talk("Ask me anything, and I will deep search wikipedia to grab answers for you")
            #    self.talk("to exit, say bye")
            #    self.talk("you should know that I take a while searching answers")
                #self.talk("When I am done searching, I will remind you about your question and answer")
            if (self.activate("자비스")):
                try:
                    self.talk('무엇을 도와드릴까요?')
                    query = self.speech2T()
                    if(query == "잘가"):
                        sp = "안녕히가세요! 종료합니다."+self.friend
                        self.talk(sp)
                        return
                    elif(query == "추가"):
                        self.talk("추가중")
                        model = self.wiki.getBertQAModelCopy(1)
                        self.wiki.models.append(model)
                        self.talk("추가완료")
                    self.talk('위키피디아 검색실행')
                    self.talk('끝나면 알려드리겠습니다!')
                    answer = self.wiki.ask(query)
                    if(answer != None):
                        self.talk(answer)
                    else:
                        self.talk("답을 못찾았어요")
                
                except sr.UnknownValueError:
                    self.talk('다시한번 반복해주시겠어요?')
                    command = speech2T()

ass = Assistant()
ass.run()
