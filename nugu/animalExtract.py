from bs4 import BeautifulSoup
import requests
import urllib
import sys
import codecs
import json

import re

url = "http://animal.memozee.com/animal/Dic/"



f=codecs.open("source.html", 'r', 'cp949')
document= BeautifulSoup(f.read()).get_text()
soup = BeautifulSoup(document, 'lxml')
print (document)
#retrieve links of search query
count = 0
animals = {"ANIMAL":{}}
intents = []

#action
#살아
#먹어
userDict = { "TARGET":{"어디": [
          "어느쪽",
          "어느 부근",
          "어느곳",
          "어떤곳",
          "어느곳이야",
          "어떤곳이야",
          "어느 부근이야",
          "어느쪽이야",
          "어디야"
        ],
        "어떻게": [
          "어때",
          "어떤거",
          "어떤거야"
        ],
        "뭐야": [
          "뭐지",
          "뭐가",
          "무엇이",
          "무엇을",
          "무엇이야"
        ],
        "언제": [
          "몇년도",
          "몇월",
          "몇일",
          "몇시",
          "몇분",
          "몇초",
          "몇달",
          "몇주",
          "어떤주",
          "어떤날"
        ]
        },"ACTION":{"살아":["서식해"],"먹어":["섭취","섭취해"]},"ANIMAL":{}
        }


for a in soup.find_all('li'):
    #print(a.attrs.keys())
    b = a.find('a').text
    print(b)
    b = re.sub(r" ?\([^)]+\)", "", b)
    userDict["ANIMAL"][b] = []

    question = "[ANIMAL:ANIMAL]"+b+"[/ANIMAL:ANIMAL]"+"가 "+"[TARGET:TARGET]"+"뭐야"+"[/TARGET:TARGET]"+"?"
    intents.append(question)
    question = "[ANIMAL:ANIMAL]"+b+"[/ANIMAL:ANIMAL]"+"가 "+"[TARGET:TARGET]"+"어디에"+"[/TARGET:TARGET]"+" "+"[ACTION:ACTION]"+"살아"+"[/ACTION:ACTION]"+"?"
    intents.append(question)
    question = "[ANIMAL:ANIMAL]"+b+"[/ANIMAL:ANIMAL]"+"가 "+"[TARGET:TARGET]"+"어디서"+"[/TARGET:TARGET]"+" "+"[ACTION:ACTION]"+"먹어"+"[/ACTION:ACTION]"+"?"
    intents.append(question)
    question = "[ANIMAL:ANIMAL]"+b+"[/ANIMAL:ANIMAL]"+"가 "+"[TARGET:TARGET]"+"무엇을"+"[/TARGET:TARGET]"+" "+"[ACTION:ACTION]"+"먹어"+"[/ACTION:ACTION]"+"?"
    intents.append(question)

    # if(count < 1000):
    #     links.append(urllib.parse.unquote(b.attrs['href']))
    #     count += 1
    #     print(urllib.parse.unquote(b.attrs['href']))
    # else:
    #     break



dict = {"taggedSentenceList":[{"animal":intents}], "userDictList":[userDict]}

with open("output.json","w") as file:
    json.dump(dict, file, ensure_ascii=False)
