var express = require('express');
var app = express();
app.listen(3000, function () {
  console.log('server running on port 3000');
})

app.get('/ask', callD_alembert);



function callD_alembert(req, res) {
  // using spawn instead of exec, prefer a stream over a buffer
  // to avoid maxBuffer issue
  var zerorpc = require("zerorpc");

  var client = new zerorpc.Client();
  client.connect("tcp://127.0.0.1:4545");
  client.invoke("ask", req.query.question, function(error, res, more) {
      return res;
  });
  res.send();
}
